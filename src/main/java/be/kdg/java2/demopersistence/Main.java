package be.kdg.java2.demopersistence;

import be.kdg.java2.demopersistence.presentation.Menu;
import be.kdg.java2.demopersistence.repository.StudentDAO;
import be.kdg.java2.demopersistence.repository.StudentDAOWithDatabase;
import be.kdg.java2.demopersistence.repository.StudentDAOWithObjects;

import java.util.logging.Logger;

public class Main {
    private static final Logger logger = Logger.getLogger(Main.class.getName());
    public static void main(String[] args) {
        logger.info("Starting application...");
        StudentDAO studentDAO = new StudentDAOWithDatabase();
        Menu menu = new Menu(studentDAO);
        menu.show();
    }
}
