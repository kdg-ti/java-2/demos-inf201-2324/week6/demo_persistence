package be.kdg.java2.demopersistence.repository;

import be.kdg.java2.demopersistence.Student;

import java.util.List;

public interface StudentDAO {
    void save(Student student);
    List<Student> loadStudents();
}
