package be.kdg.java2.demopersistence.repository;

import be.kdg.java2.demopersistence.Student;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Stream;

//Klasse verantwoordelijk voor saven en laden van studenten...
public class StudentDAOWithObjects implements StudentDAO {
    private final String STUDENT_OBJECTS = "studentobjects";
    private static final Logger logger = Logger.getLogger(StudentDAOWithObjects.class.getName());
    @Override
    public void save(Student student){
        logger.info("Saving student " + student + "...");
        try (ObjectOutputStream oos =
                new ObjectOutputStream(
                        new FileOutputStream(STUDENT_OBJECTS + "/"
                                + student.getName() + ".ser"))){
            oos.writeObject(student);
        } catch (IOException ie) {
            logger.severe("Unable to save student " + student + ":" + ie.getMessage());
            throw new RuntimeException(ie);
        }
    }

    @Override
    public List<Student> loadStudents(){
        logger.info("TODO:loading students...");
        List<Student> students = new ArrayList<>();
        try (Stream<Path> stream = Files.list(Paths.get(STUDENT_OBJECTS))) {
            List<Path> files = stream.filter(file -> !Files.isDirectory(file)).toList();
            for (Path file : files) {
                //nu "deserializen": file object terug omzetten naar java object!
                try (ObjectInputStream ois
                             = new ObjectInputStream(new FileInputStream(file.toFile()))){
                    Student student = (Student) ois.readObject();
                    students.add(student);
                }
            }
        } catch (IOException | ClassNotFoundException | ClassCastException e) {
            logger.severe("Unable to load students!" + e.getMessage());
            throw new RuntimeException(e);
        }
        return students;
    }
}
