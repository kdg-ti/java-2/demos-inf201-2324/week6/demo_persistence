package be.kdg.java2.demopersistence.repository;

import be.kdg.java2.demopersistence.Student;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class StudentDAOWithDatabase implements StudentDAO{
    private static final Logger logger = Logger.getLogger(StudentDAOWithDatabase.class.getName());
    private Connection connection;


    public StudentDAOWithDatabase() {
        try {
            connection =
                    DriverManager.getConnection("jdbc:hsqldb:file:database/students",
                            "sa","");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override

   /* public void save(Student student){
        try (Statement statement = connection.createStatement()) {
            //SUPERGEVAARLIJK!!!!!!!!
            //IK GEBRUIK USER INPUT OM MIJN QUERY OP TE BOUWEN!!!!!!
            //GEVAAR VOOR SQLINJECTIE!!!!!
            statement.executeUpdate("INSERT INTO STUDENTS(NAME, BIRTHDAY, LENGTH) " +
                    "VALUES ('" + student.getName() + "', " +
                    "'" + Date.valueOf(student.getBirthday()) + "', " + student.getLength() + ")");
        } catch (SQLException e) {
            logger.severe("Unable to save the student to the database:" + e.getMessage());
            throw new RuntimeException(e);
        }
    }*/

    public void save(Student student){
        try (PreparedStatement statement =
                     connection.prepareStatement("INSERT INTO STUDENTS(NAME, BIRTHDAY, LENGTH) VALUES(?,?,?)",Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, student.getName());
            statement.setDate(2, Date.valueOf(student.getBirthday()));
            statement.setDouble(3, student.getLength());
            statement.executeUpdate();
            ResultSet rsKeys = statement.getGeneratedKeys();
            while (rsKeys.next()) {
                student.setId(rsKeys.getInt(1));
                logger.info("Added student " + student);
            }
        } catch (SQLException e) {
            logger.severe("Unable to save the student to the database:" + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Student> loadStudents(){
        List<Student> students = new ArrayList<>();
        //stap voor stap:
        //1) juiste JDBC Driver laden --> doet gradle, in orde!
        //2) connectie maken met de database
        try (Statement statement = connection.createStatement()) {//try with resources --> automatische close!
            ResultSet resultSet = statement.executeQuery("SELECT * FROM STUDENTS");
            while (resultSet.next()) {
                long id = resultSet.getInt("ID");
                String name = resultSet.getString("NAME");
                LocalDate birthday = resultSet.getDate("BIRTHDAY").toLocalDate();
                double length = resultSet.getDouble("LENGTH");
                //4) resultaat omzetten naar lijst van studenten
                Student student = new Student(id, name, birthday, length);
                students.add(student);
            }
        } catch (SQLException e) {
            logger.severe("Unable to connect to the database:" + e.getMessage());
            throw new RuntimeException(e);
        }
        return students;
    }
}
