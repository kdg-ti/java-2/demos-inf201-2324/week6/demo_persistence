package be.kdg.java2.demopersistence;

import java.io.Serializable;
import java.time.LocalDate;

//Domain klasse Student
public class Student implements Serializable {
    private long id;
    private String name;
    private LocalDate birthday;
    private double length;

    public Student(String name, LocalDate birthday, double length) {
        this(-1, name, birthday, length);
    }

    public Student(long id, String name, LocalDate birthday, double length) {
        this.id = id;
        this.name = name;
        this.birthday = birthday;
        this.length = length;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public double getLength() {
        return length;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", birthday=" + birthday +
                ", length=" + length +
                '}';
    }
}
