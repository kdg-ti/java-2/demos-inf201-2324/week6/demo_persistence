package be.kdg.java2.demopersistence.presentation;

import be.kdg.java2.demopersistence.Student;
import be.kdg.java2.demopersistence.repository.StudentDAO;

import java.time.LocalDate;
import java.util.Scanner;

public class Menu {
    private Scanner scanner = new Scanner(System.in);
    private StudentDAO studentDAO;//Menu "depends on" StudentDAO

    public Menu(StudentDAO studentDAO){
        this.studentDAO = studentDAO;
    }

    public void show(){
        while (true) {
            System.out.print("Voeg student toe(1) of geef lijst van alle studenten(2):");
            int choice = scanner.nextInt();
            scanner.nextLine();//om de enter van de buffer te gooien
            switch (choice) {
                case 1 -> addStudent();
                case 2 -> listStudents();
                default -> System.out.println("Foute keuze...");
            }
        }
    }

    private void listStudents() {
        studentDAO.loadStudents().forEach(System.out::println);
    }

    private void addStudent() {
        System.out.print("Geef de naam:");
        String name = scanner.nextLine();
        System.out.print("Geef geboortejaar:");
        int year = scanner.nextInt();
        System.out.print("Geef geboortemaand:");
        int month = scanner.nextInt();
        System.out.print("Geef geboortedag:");
        int day = scanner.nextInt();
        System.out.print("Geef lengte:");
        double length = scanner.nextDouble();
        studentDAO.save(new Student(name, LocalDate.of(year, month, day), length));
    }
}
